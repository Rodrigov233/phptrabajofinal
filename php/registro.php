<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse</title>
    <link rel="stylesheet" href="/trabajofinal/css/login2.0.css">
</head>
<body>
<body style="display:flex; align-items:center; justify-content:center;">
    <div class="login-page">
        <div class="form">
            <form action="/trabajofinal/php/verificaregistro.php" method="POST" >
                <h2>Registro</h2>
                <input type="text" placeholder="Nombre*" name="nombre" required/>
                <input type="text" placeholder="Apellido*" name="apellido" required/>
                <input type="email" placeholder="Correo electrónico *" name="email" required/>
                <input type="password" placeholder="Contraseña *" name="contrasena" required/>
                <button class="btn" type="submit">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    Crear
                </button>
            </form>
            <p class="message">¿Ya estás registrado? <a href="/trabajofinal/php/login.php">Inicia sesión</a></p>
        </div>
    </div>
</body>
</html>
