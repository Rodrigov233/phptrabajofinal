<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Webleb</title>
    <link rel="stylesheet" href="/trabajofinal/css/login2.0.css">
</head>
<body style="display:flex; align-items:center; justify-content:center;">
<div class="login-page">
    <div class="form">
        <form action="/trabajofinal/php/verificalogin.php" method="post"> 
            <h2>Iniciar sesión</h2>
            <input type="email" placeholder="Correo electronico" name="email" required /> 
            <input type="password" placeholder="Contraseña" name="contrasena" required/>
            <button class="btn" type="submit"> 
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                Iniciar sesión
            </button>
        </form>
        <p class="message">¿No estás registrado? <a href="/trabajofinal/php/registro.php">Crea una cuenta</a></p>
    </div>
</div>
</body>
</html>
