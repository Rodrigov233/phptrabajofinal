<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/trabajofinal/css/error.css">
    <title>404 - Page Not Found</title>
    <script>
    function redirigir() {
        window.location.href = "./inicio.php";
    }
    </script>
</head>
<body>
    <div class="error-container">
        <h1 class="error-title">404</h1>
        <p class="error-text">
            Oops! Lo siento, pero no tienes acceso a esta página. 
            <span class="animate-blink" onclick="redirigir()">Vuelve atrás</span>
        </p>
    </div>
</body>
</html>
