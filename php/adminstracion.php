<?php
// Iniciar sesión y verificar el rol de administrador
session_start();

if (!isset($_SESSION['rol']) || $_SESSION['rol'] !== 'admin') {
    header("Location: /trabajofinal/php/error.php");
    exit();
}

// Conexión a la base de datos
$ip = "127.0.0.1";
$database = "carrefour";
$user = "root";
$pass = "root1234";

$conexion = mysqli_connect($ip, $user, $pass) or die ("No se ha podido conectar a la base de datos");
mysqli_select_db($conexion, $database) or die ("No existe la base de datos");

// Código para modificar y eliminar usuarios
if (isset($_POST['modificar'])) {
    $id_usuario = $_POST['id'];
    $nuevo_rol = $_POST['modificar'];
    
    $consulta_actualizar = "UPDATE usuarios SET rol = '$nuevo_rol' WHERE id = $id_usuario";
    $resultado_actualizar = mysqli_query($conexion, $consulta_actualizar);

    if ($resultado_actualizar) {
        echo "El rol del usuario ha sido actualizado correctamente.";
    } else {
        echo "Error al actualizar el rol del usuario: " . mysqli_error($conexion);
    }
}

if (isset($_POST['eliminar'])) {
    $id_usuario = $_POST['eliminar'];
    $consulta_eliminar = "DELETE FROM usuarios WHERE id = $id_usuario";
    $resultado_eliminar = mysqli_query($conexion, $consulta_eliminar);

    if ($resultado_eliminar) {
        echo "El usuario y sus datos han sido eliminados correctamente.";
    } else {
        echo "Error al eliminar el usuario y sus datos: " . mysqli_error($conexion);
    }
}

// Consulta para obtener los datos de los usuarios
$consulta = "SELECT * FROM usuarios";
$resultado = mysqli_query($conexion, $consulta);

// Interfaz HTML para mostrar y modificar usuarios
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tablero</title>
    <link rel="stylesheet" href="/trabajofinal/css/admini.css">
</head>
<body>
    <video autoplay muted loop id="videoFondo">
        <source src="/trabajofinal/imagenes/paisaje.mp4" type="video/mp4">
    </video>
    <div class="container">
        <h1>Tablero de Jugadores</h1>
        <table border="2" class="tabla">
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Rol</th>
                <th>Eliminar</th>
                <th>Modificar</th>
            </tr>
            <?php 
            while ($fila = mysqli_fetch_assoc($resultado)) { ?>
                <tr>
                    <td><?php echo $fila['id']; ?></td>
                    <td><?php echo $fila['nombre']; ?></td>
                    <td><?php echo $fila['apellido']; ?></td>
                    <td><?php echo $fila['email']; ?></td>
                    <td><?php echo $fila['rol']; ?></td>
                    <td>
                        <form method="post">
                            <input type="hidden" name="eliminar" value="<?php echo $fila['id']; ?>">
                            <button class="btn" type="submit">Eliminar</button>
                        </form>
                    </td>
                    <td>
                        <form method="post">
                            <input type="hidden" name="id" value="<?php echo $fila['id']; ?>">
                            <select class="btn2" name="modificar">
                                <option value="invitado" <?php if ($fila['rol'] === 'invitado') echo 'selected'; ?>>Invitado</option>
                                <option value="usuario" <?php if ($fila['rol'] === 'usuario') echo 'selected'; ?>>Usuario</option>
                                <option value="admin" <?php if ($fila['rol'] === 'admin') echo 'selected'; ?>>Admin</option>
                            </select>
                            <button class="btn3" type="submit">Guardar</button>
                        </form>
                    </td>
                </tr>
            <?php 
            } ?>
        </table>
        <a class="texto" href="/trabajofinal/php/inicio.php">Regresar al Inicio</a>
    </div>
</body>
</html>

<?php
// Cerrar la conexión
mysqli_close($conexion);
?>
