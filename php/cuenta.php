<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Datos de la cuenta</title>
    <link rel="stylesheet" href="/trabajofinal/css/cuenta.css">
</head>
<body>
<?php
session_start();

// Verificar si el usuario tiene un rol permitido para acceder a esta página
$roles_permitidos = ["admin", "usuario"];
if (!isset($_SESSION['rol']) || !in_array($_SESSION['rol'], $roles_permitidos)) {
    header("Location: /trabajofinal/php/error.php"); 
    exit();
}

$ip = "127.0.0.1";
$database = "carrefour";
$user = "root";
$pass = "root1234";

$conn = new mysqli($ip, $user, $pass, $database);

if ($conn->connect_error) {
    die("Error en la conexión a la base de datos: " . $conn->connect_error);
}
// Definir la función para mostrar mensajes de error
function mostrarError($mensaje) {
    echo "<div style='color: red; font-weight: bold;'>Error: $mensaje</div>";
}


$id_usuario = $_SESSION['id_usuario'];

// Consulta para obtener datos del usuario
$sql = "SELECT * FROM usuarios WHERE id = $id_usuario";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $usuario = $result->fetch_assoc();

    echo "<div class='caja'>";
    echo "<h1>Bienvenido, " . $usuario["nombre"] . " " . $usuario["apellido"] . "!</h1>";
    echo "<img class='imagen' src='" . $usuario["foto_predeterminada"] . "' alt='Foto del usuario' width='100'>";
    echo "<p>Nombre: " . $usuario["nombre"] . "</p>";
    echo "<p>Apellido: " . $usuario["apellido"] . "</p>";
    echo "<p>Email: " . $usuario["email"] . "</p>";
    echo "</div>";

    // Formulario para cambiar imagen de perfil
    echo "<form action='/trabajofinal/php/imagen.php' method='post' enctype='multipart/form-data'>";
    echo "<h2>Cambiar Imagen de Perfil</h2>"; 
    echo "<input type='file' name='nueva_imagen' required>"; 
    echo "<button type='submit'>Cambiar Imagen</button>"; 
    echo "</form>";

    // Formulario para modificar datos del usuario
    echo "<h2>Modificar Datos del Usuario</h2>";
    echo "<form action='/trabajofinal/php/cuenta.php' method='post' enctype='multipart/form-data'>";
    echo "<input type='hidden' name='id_usuario' value='" . $id_usuario . "'>"; 
    echo "<select name='campo' required>"; 
    echo "<option value='nombre'>Nombre</option>";
    echo "<option value='apellido'>Apellido</option>";
    echo "<option value='email'>Correo electrónico</option>";
    echo "<option value='contrasena'>Contraseña</option>";
    echo "<option value='foto'>Foto</option>"; 
    echo "</select>";
    echo "<input type='text' name='nuevo_valor' placeholder='Nuevo valor' required>"; 
    echo "<input type='password' name='contrasena_confirmacion' placeholder='Confirmar contraseña' required>"; 
    echo "<button type='submit' name='submit'>Guardar Cambios</button>"; 
    echo "</form>";

    // Procesar el formulario
    if (isset($_POST['submit'])) {
        $campo = $_POST['campo'];
        $nuevo_valor = $_POST['nuevo_valor'];
        $contrasena_confirmacion = $_POST['contrasena_confirmacion'];

        // Verificar la contraseña actual del usuario
        $sql_password = "SELECT contrasena FROM usuarios WHERE id = $id_usuario";
        $result_password = $conn->query($sql_password);

        if ($result_password->num_rows > 0) {
            $row_password = $result_password->fetch_assoc();
            $hashed_password = $row_password['contrasena'];

            if (password_verify($contrasena_confirmacion, $hashed_password)) {
                if ($campo === 'foto') {
                    if ($_FILES['nueva_foto']['error'] === UPLOAD_ERR_OK) {
                        $nombre_temporal = $_FILES['nueva_foto']['tmp_name'];
                        $extension = pathinfo($_FILES['nueva_foto']['name'], PATHINFO_EXTENSION);
                        $nueva_ruta = "/trabajofinal/imagenes/" . $id_usuario . "." . $extension;

                        if (move_uploaded_file($nombre_temporal, $_SERVER['DOCUMENT_ROOT'] . $nueva_ruta)) {
                            $sql_update = "UPDATE usuarios SET foto_predeterminada = '$nueva_ruta' WHERE id = $id_usuario";

                            if ($conn->query($sql_update) === TRUE) {
                                echo "<meta http-equiv='refresh' content='0'>"; // Refrescar para mostrar la nueva imagen
                            } else {
                                mostrarError("Error al actualizar la foto: " . $conn->error);
                            }
                        } else {
                            mostrarError("Error al mover la nueva foto.");
                        }
                    } else {
                        mostrarError("Error al cargar la nueva foto: " . $_FILES['nueva_foto']['error']);
                    }
                } else {
                    // Actualizar otros campos
                    $sql_update = "UPDATE usuarios SET $campo = '$nuevo_valor' WHERE id = $id_usuario";

                    if ($conn->query($sql_update) === TRUE) {
                        echo "<meta http-equiv='refresh' content='0'>"; // Actualizar para mostrar cambios
                    } else {
                        mostrarError("Error al actualizar el campo '$campo': " . $conn->error);
                    }
                }
            } else {
                mostrarError("Contraseña incorrecta. No se pueden realizar cambios.");
            }
        } else {
            mostrarError("No se pudo obtener la contraseña del usuario.");
        }
    }

    $conn->close();
} else {
    echo "No se encontraron datos del usuario.";
}
?>
</body>
</html>
