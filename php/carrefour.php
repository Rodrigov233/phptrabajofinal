<?php
session_start(); // Iniciar sesión para acceder a las variables de sesión

// Verificar si el usuario tiene un rol permitido
$roles_permitidos = ["admin", "usuario"];

if (!isset($_SESSION['rol']) || !in_array($_SESSION['rol'], $roles_permitidos)) {
    // Redirigir si el usuario no tiene un rol permitido
    header("Location: /trabajofinal/php/error.php");
    exit(); // Detener el script para evitar contenido no autorizado
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carrefour</title>
    <!-- Enlace al swiper js-->
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

    <!-- Enlace al CDN de Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <!-- Enlace al archivo CSS personalizado -->
    <link rel="stylesheet" href="/trabajofinal/css/carrefour1.css">
    <!--=============== FAVICON ===============-->
    <link rel="shortcut icon" href="/trabajofinal/imagenes/carrefour-removebg-preview.png" type="image/x-icon">
</head>

<!-- Sección de encabezado -->

<header class="header">

    <section class="flex">
        <a href="/trabajofinal/php/inicio.php" class="logo"> <i class="fas fa-shopping-basket"></i> Carrefour </a>
        <!-- Menú de navegación -->
    <nav class="navbar">
        <a href="#home">Inicio</a>
        <a href="#features">caracteristicas</a>
        <a href="#products">productos</a>
        <a href="#categories">categorias</a>
        <a href="#review">reseñas</a>
        <a href="#blogs">blogs</a>
        
    </nav>
        <!-- Iconos para acciones -->
    <div class="icons">
        <div class="fas fa-bars" id="menu-btn"></div>
        <div class="fas fa-search" id="search-btn"></div>
        <a href="./adminstracion.php" class="boton"><div class="fas fa-user-secret"></div></a>
        <a href="./cuenta.php" class="boton"><div class="fas fa-user"></div></a>
        
    </div>

</header>

<!-- Sección de encabezado final -->

<!-- Sección de inicio -->

<div class="home-container">

    <section class="home" id="home">

        <div class="content">
            
            <h3>Calidad y precio<span> en nuestro juego.</span>¡Descúbrelo ya!</h3>
            <p>"¡Desafía tus conocimientos con nuestro nuevo juego! ¿Cuánto sabes sobre Carrefour? ¡Demuestra tus habilidades y comparte tu puntaje! ¡Juega ahora y conviértete en un experto en Carrefour!"</p>
            <a href="./quiz.html" class="btn">Juega Ahora!</a>
        </div>
    
    </section>

</div>

<!-- Sección de inicio  final -->

<!-- Sección de características -->

<section class="features" id="features">

    <h1 class="heading"> Nuestras <span>caracteristicas</span> </h1>

    <div class="box-container">

        <div class="box">
            <img src="/trabajofinal/imagenes/Captura_de_pantalla_2024-01-04_a_la_s__23.29.49-removebg-preview.png" alt="">
            <h3>frescos y organicos</h3>
            <p>En nuestro mercado, la frescura cobra vida. Desde exuberantes manzanas hasta jugosas fresas, cada fruta organica es un estallido de vitalidad para un cuerpo joven y sano.</p>
            <a href="./frutas.html" class="btn">Leer Más</a>
        </div>

        <div class="box">
            <img src="/trabajofinal/imagenes/a-removebg-preview.png">
            <h3>delivery gratis</h3>
            <p>Nuestro servicio de delivery redefine la conveniencia. Desde la comodidad de tu hogar hasta la puerta de tu residencia, llevamos la excelencia directamente a tus manos.</p>
            <a href="./delivery.html" class="btn">Leer Más</a>
        </div>

        <div class="box">
            <img src="/trabajofinal/imagenes/formas-pago.png" alt="">
            <h3>pagos faciles</h3>
            <p> Ofrecemos una variedad de métodos de pago que se adaptan a tus preferencias. Desde pagos en efectivo hasta las últimas opciones digitales, facilitamos cada transacción. </p>
            <a href="./pago.html" class="btn">Leer Más</a>
        </div>

    </div>

</section>

<!-- Sección de características final -->

        <!-- Sección de productos inicio -->
<section class="products" id="products">

    <h1 class="heading"> Nuestros <span>productos</span> </h1>

    <div class="swiper product-slider">

        <div class="swiper-wrapper">

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/naranja_g-removebg-preview.png" alt="">
                <h3>Naranja Fresca</h3>
                <div class="price"> €0.99/- - 4.99/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">añadir a la cesta</a>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/saco-de-cebollas-10-kg-removebg-preview.png" alt="">
                <h3>Cebolla fresca</h3>
                <div class="price"> €2.99/- - 6.99/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">añadir a la cesta</a>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/gratis-png-carne-de-cerdo-ternera-orloff-costillas-de-res-solomillo-fresco-removebg-preview.png"">
                <h3>Carne fresca</h3>
                <div class="price"> €4.99/- - 11.99/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">añadir a la cesta</a>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/pollo-removebg-preview.png" alt="">
                <h3>Pollo fresco</h3>
                <div class="price"> €4.99/- - 10.99/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">añadir a la cesta</a>
            </div>

        </div>

    </div>

    <div class="swiper product-slider">

        <div class="swiper-wrapper">

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/gratis-png-patatas-marrones-patata-vegetal-zanahoria-papas-papas-thumbnail-removebg-preview.png" alt="">
                <h3>Patata fresca</h3>
                <div class="price"> €3.99/- - 8.99/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">añadir a la cesta</a>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/product-6.png" alt="">
                <h3>Palta fresca</h3>
                <div class="price"> €2.99/- - 4.99/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">añadir a la cesta</a>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/Carrot-PNG-Clipart.png" alt="">
                <h3>Zanahoria</h3>
                <div class="price"> €3.99/- - 6.99/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">añadir a la cesta</a>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/limon-removebg-preview.png" alt="">
                <h3>Limon verde</h3>
                <div class="price"> €1.99/- - 3.99/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">añadir a la cesta</a>
            </div>

        </div>

    </div>


</section>

    <!-- Sección de productos final -->

<!-- Sección de categorías inicio -->

<section class="categories" id="categories">

    <h1 class="heading"> Categorias de<span>Productos</span> </h1>

    <div class="box-container">

        <div class="box">
            <img src="/trabajofinal/imagenes/no-background-tarjetas-colores_2.png" alt="">
            <h3>carrefour tarjetas acccesibles pass</h3>
            <p>upto 15% off</p>
            <a href="#" class="btn">Comprar ahora</a>
        </div>

        <div class="box">
            <img src="/trabajofinal/imagenes/electrodomesticos-hogar-removebg-preview.png">
            <h3>Productos electrodomesticos</h3>
            <p>upto 55% off</p>
            <a href="#" class="btn">Comprar ahora</a>
        </div>

        <div class="box">
            <img src="/trabajofinal/imagenes/Ene-Descubre-como-puedes-aplicar-la-decoracion-con-plantas-para-darle-nueva-vida-a-tus-espacios-scaled-removebg-preview.png" alt="">
            <h3>Productos para el hogar</h3>
            <p>upto 25% off</p>
            <a href="#" class="btn">Comprar ahora</a>
        </div>

        <div class="box">
            <img src="/trabajofinal/imagenes/comprar-moda-sostenible-removebg-preview.png">
            <h3>Productos para la moda</h3>
            <p>upto 35% off</p>
            <a href="#" class="btn">Comprar ahora</a>
        </div>

    </div>

</section>

<!-- Sección de categorías  final-->

<!-- Sección de reseñas inicio -->

<section class="review" id="review">

    <h1 class="heading"> Reseña de la <span>clientela</span> </h1>

    <div class="swiper review-slider">

        <div class="swiper-wrapper">

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/MicrosoftTeams-image (15).png" alt="">
                <p>La calidad de las frutas y verduras en este mercado es insuperable. Cada vez que compro aquí, sé que estoy llevando a casa productos frescos y saludables.</p>
                <h3>Ana de armas</h3>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/MicrosoftTeams-image (16).png" alt="">
                <p>Este mercado es simplemente increíble. Siempre encuentro una amplia variedad de frutas y verduras frescas.La calidad es inigualable, y la abundancia de opciones. </p>
                <h3>john degle</h3>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/MicrosoftTeams-image (17).png" alt="">
                <p>El personal siempre está dispuesto a ayudar y ofrecer recomendaciones. Siempre me siento bienvenido y apreciado como cliente y sobre todo la amabilidad. </p>
                <h3>Andres wiese</h3>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
            </div>

            <div class="swiper-slide box">
                <img src="/trabajofinal/imagenes/MicrosoftTeams-image (18).png" alt="">
                <p>El mercado siempre está impecablemente limpio y organizado. La presentación de los productos es atractiva,Un ambiente limpio y ordenado.</p>
                <h3>Elizabeth olsen</h3>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
            </div>

        </div>

    </div>

</section>

<!-- Sección de reseñas final -->

<!-- Sección de blogs inicio-->

<section class="blogs" id="blogs">

    <h1 class="heading"> Nuestros <span>blogs</span> </h1>

    <div class="box-container">

        <div class="box">
            <img src="/trabajofinal/imagenes/flamengo.jpg" alt="">
            <div class="content">
                <div class="icons">
                    <a href="#"> <i class="fas fa-user"></i> by Carrefour </a>
                    <a href="#"> <i class="fas fa-calendar"></i> 9 noviembre, 2023 </a>
                </div>
                <h3>DÍA INTERNACIONAL DEL FLAMENCO</h3>
                <p>El pasado jueves 16 de noviembre, fue el día internacional del Flamenco. La Consejería de Cultura, Turismo y Deporte.</p>
                <a href="#" class="btn">Leer Más </a>
            </div>
        </div>

        <div class="box">
            <img src="/trabajofinal/imagenes/cata-550x654.jpg" alt="">
            <div class="content">
                <div class="icons">
                    <a href="#"> <i class="fas fa-user"></i> by Carrefour </a>
                    <a href="#"> <i class="fas fa-calendar"></i> 22 de noviembre, 2023 </a>
                </div>
                <h3>CATA DE VINOS CON MARIDAJE DE QUESOS</h3>
                <p>El próximo miércoles 29 de noviembre a las 18:00h, viviremos una experiencia maravillosa.</p>
                <a href="#" class="btn">Leer Más</a>
            </div>
        </div>

        <div class="box">
            <img src="/trabajofinal/imagenes/navidad.jpeg">
            <div class="content">
                <div class="icons">
                    <a href="#"> <i class="fas fa-user"></i> by Carrefour </a>
                    <a href="#"> <i class="fas fa-calendar"></i> 18 de diciembre, 2023 </a>
                </div>
                <h3>24 RAZONES PARA DISFRUTAR DE LA NAVIDAD</h3>
                <p>Nos sobran razones para disfrutar de la Navidad, y a nuestro Mercado le sobran puestos para saborearla.</p>
                <a href="#" class="btn">Leer Más </a>
            </div>
        </div>

    </div>

</section>

<!-- Sección de blogs  final-->



<!-- Sección del footer inicio -->
<section class="footer">

    <div class="box-container">

        <div class="box">
            <h3> Carrefour <i class="fas fa-shopping-basket"></i> </h3>
            <a href="https://www.carrefour.es/contacta-con-nosotros.e" class="links"> <i class="fas fa-paperclip"></i> Carrefour en España </a>
            <a href="https://www.carrefour.es/contacta-con-nosotros.e" class="links"> <i class="fas fa-paperclip"></i> Trabaja con nosotros </a>
            <a href="https://www.carrefour.es/contacta-con-nosotros.e" class="links"> <i class="fas fa-paperclip"></i> Club carrefour </a>
            <div class="share">
                <a href="https://www.facebook.com/carrefoures"target="_blank" class="fab fa-facebook-f"></a>
                <a href="https://twitter.com/CarrefourES" target="_blank" class="fab fa-twitter"></a>
                <a href="https://www.instagram.com/carrefoures/" target="_blank" class="fab fa-instagram"></a>
                <a href="https://www.linkedin.com/company/carrefoures/"target="_blank" class="fab fa-linkedin"></a>
            </div>
        </div>

        <div class="box">
            <h3>Información De Contacto</h3>
            <a href="https://www.google.com/search?q=carrefour+numero+atencion+al+cliente&oq=carrefour+nume&gs_lcrp=EgZjaHJvbWUqBwgCEAAYgAQyCQgAEEUYOxiABDIGCAEQRRg5MgcIAhAAGIAEMgcIAxAAGIAEMgcIBBAAGIAEMgcIBRAAGIAEMgcIBhAAGIAE0gEIOTcwMGowajGoAgCwAgA&sourceid=chrome&ie=UTF-8#" class="links"> <i class="fas fa-phone"></i> +34 916 63 32 04 </a>
            <a href="https://www.google.com/search?q=carrefour+numero+atencion+al+cliente&oq=carrefour+nume&gs_lcrp=EgZjaHJvbWUqBwgCEAAYgAQyCQgAEEUYOxiABDIGCAEQRRg5MgcIAhAAGIAEMgcIAxAAGIAEMgcIBBAAGIAEMgcIBRAAGIAEMgcIBhAAGIAE0gEIOTcwMGowajGoAgCwAgA&sourceid=chrome&ie=UTF-8#" class="links"> <i class="fas fa-phone"></i> +34 914 90 89 00 </a>
            <a href="clientes_carrefour.es@carrefour.com" class="links"> <i class="fas fa-envelope"></i>clientes_carrefour@email.com</a>
            <a href="https://www.google.es/maps/place/Carrefour/@40.4181562,-3.6413596,15z/data=!3m1!5s0xd422f81d4f624f1:0xe8c76fea33cf5a3c!4m10!1m2!2m1!1sCarrefour!3m6!1s0xd422f8197ddbe75:0xa53118f359f7775c!8m2!3d40.4181572!4d-3.6219044!15sCglDYXJyZWZvdXIiA4gBAVoLIgljYXJyZWZvdXKSAQtoeXBlcm1hcmtldOABAA!16s%2Fg%2F11xmzjjvd?entry=ttu" class="links"> <i class="fas fa-map-marker-alt"></i> Madrid Av.de Guadalajara-28037 </a>
        </div>

        <div class="box">
            <h3>Enlaces Rápidos</h3>
            <a href="#home" class="links"> <i class="fas fa-arrow-right"></i> Inicio </a>
            <a href="#features" class="links"> <i class="fas fa-arrow-right"></i> caracteristicas </a>
            <a href="#products" class="links"> <i class="fas fa-arrow-right"></i> productos </a>
            <a href="#categories" class="links"> <i class="fas fa-arrow-right"></i> categorias </a>
            <a href="#review" class="links"> <i class="fas fa-arrow-right"></i> reseñas </a>
            <a href="#blogs" class="links"> <i class="fas fa-arrow-right"></i> blogs </a>
        </div>

        <div class="box">
            <h3> Boletin informativo</h3>
            <p>Suscríbete Para Recibir Las Últimas Actualizaciones</p>
            <input type="email" placeholder="Tu correo electronico" class="email">
            <input type="submit" value="subscribete" class="btn">
            <img src="/trabajofinal/imagenes/paypal.png" class="payment-img" alt="">
        </div>
    </div>
    <div class="credit"> Creador <span>DANIEL DIEGO  RODRIGO</span> |Trabajador de Carrefour🤫 </div>
    
<!-- Scripts -->
<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
<script src="/trabajofinal/js/carrefour.js"></script>

</body>
</html>