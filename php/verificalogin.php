<?php
session_start(); // Iniciar sesión para acceder a las variables de sesión

$ip = "127.0.0.1";
$database = "carrefour";
$user = "root";
$pass = "root1234";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST['email'];
    $contrasena = $_POST['contrasena'];

    $conn = new mysqli($ip, $user, $pass, $database);

    if ($conn->connect_error) {
        die("Error en la conexión: " . $conn->connect_error);
    }

    $sql = "SELECT id, contrasena, rol FROM usuarios WHERE email='$email'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $usuario = $result->fetch_assoc();

        if (password_verify($contrasena, $usuario['contrasena'])) {
            // Guarda el ID y el rol del usuario en la sesión
            $_SESSION['id_usuario'] = $usuario['id'];
            $_SESSION['rol'] = $usuario['rol'];

            
            if ($usuario['rol'] === 'admin') {
                header("Location: /trabajofinal/php/adminstracion.php");
            } elseif ($usuario['rol'] === 'usuario') {
                header("Location: /trabajofinal/php/carrefour.php");
            } elseif ($usuario['rol'] === 'invitado') {
                header("Location: /trabajofinal/php/inicio.php");
            }

            exit(); 
        } else {
            $_SESSION['error'] = "Contraseña incorrecta";
            header("Location: /trabajofinal/php/login.php");
            exit();
        }
    } else {
        $_SESSION['error'] = "Usuario no encontrado";
        header("Location: /trabajofinal/php/login.php");
        exit();
    }

    $conn->close(); // Cerrar conexión
} else {
    header("Location: /trabajofinal/php/login.php");
    exit();
}
?>
