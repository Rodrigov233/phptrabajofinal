<?php
if (session_status() === PHP_SESSION_NONE) {
    session_start(); 
}

function verificarPermiso($rol_permitido) {
    if (!isset($_SESSION['rol']) || $_SESSION['rol'] !== $rol_permitido) {
        // Redirigir si el usuario no tiene el rol permitido
        header("Location: /trabajofinal/php/error.php");
        exit();
    }
}
?>
