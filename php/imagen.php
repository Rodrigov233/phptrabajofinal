<?php
session_start(); // Asegurarse de que la sesión esté iniciada

// Conexión a la base de datos
$ip = "127.0.0.1";
$database = "carrefour";
$user = "root";
$pass = "root1234";

$conn = new mysqli($ip, $user, $pass, $database);

if ($conn->connect_error) {
    die("Error en la conexión a la base de datos: " . $conn->connect_error);
}

$id_usuario = $_SESSION['id_usuario']; // Obtener el ID del usuario actual

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nueva_imagen = $_FILES["nueva_imagen"]; // Archivo subido

    if ($nueva_imagen["error"] === 0) {
        $nombre_imagen = "perfil_" . $id_usuario . "_" . basename($nueva_imagen["name"]); 
        $ruta_imagen = "/trabajofinal/imagenes/" . $nombre_imagen;

        if (move_uploaded_file($nueva_imagen["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $ruta_imagen)) {
            // Actualizar la base de datos con la nueva imagen
            $sql_update = "UPDATE usuarios SET foto_predeterminada = '$ruta_imagen' WHERE id = $id_usuario";

            if ($conn->query($sql_update) === TRUE) {
                echo "Imagen actualizada exitosamente."; // Mensaje de éxito
            } else {
                echo "Error al actualizar la imagen: " . $conn->error; // Error al actualizar la base de datos
            }
        } else {
            echo "Error al mover la imagen al servidor."; // Error al mover la imagen
        }
    } else {
        echo "Error al cargar la imagen: " . $nueva_imagen["error"]; // Manejo de errores al cargar la imagen
    }
}

$conn->close(); // Cerrar conexión a la base de datos
?>
