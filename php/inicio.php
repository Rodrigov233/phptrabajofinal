
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--========== BOX ICONS ==========-->
        <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>

        <!--========== CSS ==========-->
        <link rel="stylesheet" href="/trabajofinal/css/inicio.css">

        <title>Inicio</title>
    </head>
    <body>

        <!--========== HEADER ==========-->
        <header class="l-header" id="header">
            <nav class="nav bd-container">
                <a href="#" class="nav__logo">Carrefour</a>

                <div class="nav__menu" id="nav-menu">
                    <ul class="nav__list">
                        <li class="nav__item"><a href="./inicio.php" class="nav__link active-link">Inicio</a></li>
                        <li class="nav__item"><a href="./registro.php" class="nav__link">Registrarse</a></li>
                        <li class="nav__item"><a href="./login.php" class="nav__link">Inciar Sesion</a></li>
                        <li class="nav__item"><a href="./cuenta.php" class="nav__link">Cuenta</a></li>
                        <li class="nav__item"><a href="./adminstracion.php"class="nav__link">administradores</a></li>

                        
                    </ul>
                </div>

                <div class="nav__toggle" id="nav-toggle">
                    <i class='bx bx-menu'></i>
                </div>
            </nav>
        </header>

        <main class="l-main">
            <!--========== HOME ==========-->
            <section class="home" id="home">
                <div class="home__container bd-container bd-grid">
                    <div class="home__data">
                        <h1 class="home__title">Carrefour España</h1>
                        <h2 class="home__subtitle">Accede a nuestros productos <br> de primera.</h2>
                        <a href="/trabajofinal/php/carrefour.php" class="button">Aceder ahora!</a>
                    </div>
    
                    <img src="/trabajofinal/imagenes/trabajador2.jpeg" alt="" class="home__img">
                </div>
            </section>
            