<?php
// Iniciar sesión
session_start();

$ip = "127.0.0.1";
$database = "carrefour";
$user = "root";
$pass = "root1234";

// Verificar si se recibieron datos del formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener los datos del formulario
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $email = $_POST['email'];
    $contrasena = $_POST['contrasena'];

    // Conectar a la base de datos
    $conn = new mysqli($ip, $user, $pass, $database);

    // Verificar la conexión
    if ($conn->connect_error) {
        die("Error en la conexión: " . $conn->connect_error);
    }

    // Comprobar si el email ya existe
    $sql = "SELECT id FROM usuarios WHERE email='$email'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Si el email ya existe, redirigir al registro
        $_SESSION['error'] = "El email ya está registrado";
        header("Location: /trabajofinal/php/registro.php");
        exit();
    } else {
        // Si el email no existe, guardar el nuevo usuario
        $hashed_password = password_hash($contrasena, PASSWORD_DEFAULT);
        $sql = "INSERT INTO usuarios (nombre, apellido, email, contrasena) VALUES ('$nombre', '$apellido', '$email', '$hashed_password')";

        if ($conn->query($sql) === TRUE) {
            // Obtener el ID y el rol del nuevo usuario
            $id_usuario = $conn->insert_id;

            // Guardar el ID y el rol del usuario en la sesión
            $_SESSION['id_usuario'] = $id_usuario;
            $_SESSION['rol'] = 'usuario'; 

            // Redirigir a la página de inicio de sesión
            header("Location: /trabajofinal/php/login.php");
            exit();
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }

    $conn->close();
} else {
    header("Location: /trabajofinal/php/registro.php");
    exit();
}
?>
