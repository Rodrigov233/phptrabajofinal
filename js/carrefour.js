

var swiper = new Swiper(".product-slider", {
    loop:true,
    spaceBetween: 15, 
    autoplay: {
        delay: 7500, 
        disableOnInteraction: false, 
    },
    centeredSlides: true, 
    breakpoints: { 
      0: {
        slidesPerView: 1, 
      },
      768: {
        slidesPerView: 2, 
      },
      1020: {
        slidesPerView: 3, 
      },
    },
});

// Configura otro deslizador para reseñas con la misma instancia de Swiper
var swiper = new Swiper(".review-slider", {
    // Las configuraciones son igualitos al deslizador de productos
    loop:true,
    spaceBetween: 15,
    autoplay: {
        delay: 7500,
        disableOnInteraction: false,
    },
    centeredSlides: true,
    breakpoints: {
      0: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 2,
      },
      1020: {
        slidesPerView: 3,
      },
    },
});
