
USE carrefour;

CREATE TABLE usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    contrasena VARCHAR(255) NOT NULL,
    rol VARCHAR(20) NOT NULL DEFAULT 'usuario'
);
ALTER TABLE usuarios ADD COLUMN foto_predeterminada VARCHAR(255) DEFAULT '/trabajofinal/imagenes/foto.jpeg';

select * from usuarios;

CREATE TABLE IF NOT EXISTS fotos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT,
    ruta VARCHAR(255),
    FOREIGN KEY (id_usuario) REFERENCES usuarios(id)
);
-- Crear la tabla de productos
create table  productos (
  id_producto int auto_increment primary key,
  nombre_producto varchar(100) not null,
  descripcion varchar(255),
  precio decimal(10, 2) not null,
  fecha_caducidad date,
  cantidad_lotes int
);
 
-- Insertar datos en la tabla de productos
insert into productos (nombre_producto, descripcion, precio, fecha_caducidad, cantidad_lotes)
values
('pollo fresco', 'Pollo fresco de granja', 10.99, '2024-06-30', 50),
('naranja fresca', 'Naranjas frescas recién recolectadas', 1.99, '2024-05-15', 100),
('cebolla fresca', 'Cebollas frescas y sabrosas', 0.99, NULL, NULL),
('carne fresca', 'Carne fresca de res', 15.99, NULL, NULL),
('patata fresca', 'Patatas frescas cultivadas localmente', 2.49, NULL, NULL),
('limon verde', 'Limones verdes jugosos', 0.79, NULL, NULL),
('aguacate', 'Aguacates maduros y cremosos', 3.99, NULL, NULL),
('zanahoria', 'Zanahorias frescas y crujientes', 1.29, NULL, NULL);
 
-- Crear la tabla de categorías
create table if not exists categorias (
  id_categoria int auto_increment primary key,
  nombre_categoria varchar(50) not null
);
 
-- Insertar datos en la tabla de categorías
insert into categorias (nombre_categoria)
values 
('frutas'),
('verduras'),
('carnes');
 
SELECT usuarios.id, usuarios.nombre, usuarios.apellido, usuarios.email, usuarios.rol, fotos.ruta AS foto
FROM usuarios
INNER JOIN fotos ON usuarios.id = fotos.id_usuario;